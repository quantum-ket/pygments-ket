[![PyPI](https://img.shields.io/pypi/v/pygments-ket.svg)](https://pypi.org/project/pygments-ket/)

# Pygments Ket

Pygments support for the Ket quantum programming language.

-----------

This project is part of the Ket Quantum Programming, see the documentation for
more information https://quantum-ket.gitlab.io.
